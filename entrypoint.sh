#!/bin/bash

set -ex

AEROO_DOCS_ARGS="--no-daemon --log-file=/tmp/aeroo_docs.log --oo-server=libreoffice --interface=0.0.0.0 --port=8989"

function wait_port {
    host=$1
    port=$2

    echo "Waiting for available tcp connection on ${host}:${port} "
    while ! /bin/nc -w 1 $host $port 2>/dev/null; do
        echo -n .
        sleep 1
    done
    echo " ok"
}

case "$1" in
    aeroo)
        shift
        wait_port libreoffice 8100
        exec /usr/bin/python3 /opt/aeroo/aeroo-docs start $AEROO_DOCS_ARGS "$@"
        ;;
    libreoffice)
        shift
        exec /usr/bin/libreoffice --nologo --nofirststartwizard --headless --norestore --invisible --accept="socket,host=0.0.0.0,port=8100,tcpNoDelay=1;urp;" "$@"
        ;;
    *)
        exec "$@"
esac

exit 1
