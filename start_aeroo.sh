#!/bin/sh

set -ex

IMG=registry.gitlab.com/apertoso/docker/aeroo:5.2.7-0.0.2
SERVICES="libreoffice aeroo"

for service in ${SERVICES}; do
    docker run \
        --detach \
        --restart=always \
        --network docker_backend \
        --name=${service} \
        --hostname=${service} \
        ${IMG} \
        ${service}
done
