# Debian 8 + necessary APT packages for Aeroo Docs

FROM debian:stretch
MAINTAINER Apertoso N.V. <info@apertoso.be>

USER root

RUN set -ex \
    # Add contrib and non-free apt sources
    && sed -i~ \
        -e's/stretch main$/stretch main contrib non-free/' \
        /etc/apt/sources.list \
    && rm /etc/apt/sources.list~ \
    # Update APT cache & install upgrades
    && apt-get update \
    && apt-get upgrade -y \

    # Install APT packages dependencies
    && apt-get install -y --no-install-recommends \
        curl \
        python3-pip \
        netcat \
        python3-uno \
        libreoffice-calc \
        libreoffice-writer \
        openjdk-8-jre \
        fontconfig \
        fontconfig-config \
        fonts-dejavu \
        fonts-dejavu-core \
        fonts-dejavu-extra \
        fonts-font-awesome \
        fonts-liberation \
        fonts-opensymbol \
        fonts-stix \
        libfontconfig1 \
        libfontenc1 \
        libfreetype6 \
        libxfont1 \
        ttf-liberation \
        ttf-mscorefonts-installer \

    && pip3 install \
        jsonrpc2 \
        daemonize \

    #Create odoo user & group
    && addgroup --system --gid 999 aeroo \
    && adduser --system --home /opt/aeroo --shell /usr/sbin/nologin --uid 999 --gid 999 --disabled-password --disabled-login aeroo \

    #GIT clone Aeroo from github
    && mkdir -p /opt/aeroo \
    && curl -Ls https://github.com/aeroo/aeroo_docs/archive/master.tar.gz | tar xz --strip-components 1 -C /opt/aeroo \

    # Clean APT cache
    && apt-get clean

ADD entrypoint.sh /

USER 999
EXPOSE 8989 8100
ENTRYPOINT ["/entrypoint.sh"]
